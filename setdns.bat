@echo off

REM This batch file configures DNS server addresses for optimal internet connectivity.
REM These settings utilize Google's DNS servers.
REM Compatible with Windows 10.

REM Define the DNS server addresses
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -Command "Set-DnsClientServerAddress -InterfaceIndex (Get-NetAdapter | Where-Object {$_.Status -eq 'Up'}).ifIndex -ServerAddresses ('8.8.8.8', '8.8.4.4')"

